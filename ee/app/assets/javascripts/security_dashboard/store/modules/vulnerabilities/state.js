export default () => ({
  hasError: false,
  isLoadingVulnerabilities: true,
  isLoadingVulnerabilitiesCount: true,
  pageInfo: {},
  vulnerabilities: [],
  vulnerabilitiesCount: {},
  vulnerabilitiesCountEndpoint: null,
  vulnerabilitiesEndpoint: null,
});
